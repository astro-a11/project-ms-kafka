package com.wu.assessment.payments.controller;

import javax.validation.Valid;
import lombok.AllArgsConstructor;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import com.wu.assessment.payments.model.Payments;
import com.wu.assessment.payments.service.PaymentsService;

@RestController
@RequestMapping("/api/v1/payments")
@AllArgsConstructor
public class PaymentsController {

  private final PaymentsService service;

  @PostMapping
  public ResponseEntity<Payments> createPayment(@RequestBody @Valid Payments payment) {

    return ResponseEntity.status(HttpStatus.CREATED)
        .body(service.save(payment));
  }

  @GetMapping("{idPayment}")
  public ResponseEntity<Payments> getPayment(@PathVariable Long idPayment) {

    return ResponseEntity.status(HttpStatus.OK)
        .body(service.getById(idPayment));
  }
}
