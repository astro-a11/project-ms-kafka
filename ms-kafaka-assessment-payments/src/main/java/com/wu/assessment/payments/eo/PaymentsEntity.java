package com.wu.assessment.payments.eo;

import java.time.LocalDate;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@Builder
@NoArgsConstructor
@AllArgsConstructor
@Entity
@Table(name = "PAYMENT")
public class PaymentsEntity {

  @Id
  @GeneratedValue(strategy = GenerationType.IDENTITY)
  @Column(name = "ID_PAYMENT")
  private Long idPayment;

  @Column(name = "ID_CUSTOMER")
  private Long idCustomer;

  @Column(name = "NAME")
  private String name;
  @Column(name = "CARD_NUMBER")
  private Long cardNumber;
  @Column(name = "EXPIRY_DATE")
  private LocalDate expiryDate;
  @Column(name = "CVV")
  private Short cvv;

}
