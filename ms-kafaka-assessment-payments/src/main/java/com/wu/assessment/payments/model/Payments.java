package com.wu.assessment.payments.model;

import javax.validation.Valid;
import javax.validation.constraints.NotNull;
import lombok.Builder;
import lombok.Data;
import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;

@Data
@Builder
public class Payments {

  @NotNull
  private Long idCustomer;

  private Long idPayment;
  
  @Valid
  private CreditCard creditcard;

  @Override
  public String toString() {

    try {
      return new ObjectMapper().writeValueAsString(this);
    } catch (JsonProcessingException exc) {
      return "";
    }
  }
  
}
