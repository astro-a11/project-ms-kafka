package com.wu.assessment.payments.eo;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.OneToOne;
import javax.persistence.Table;
import javax.validation.constraints.NotBlank;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@Builder
@NoArgsConstructor
@AllArgsConstructor
@Table(name = "RECEIVER_ENTITY")
@Entity
public class ReceiverEntity {

  @Id
  @Column(name = "ID_RECEIVER")
  private Integer idReceiver;

  @NotBlank
  private String name;
  @NotBlank
  private String address;

  @OneToOne
  @JoinColumn(name = "customer")
  private CustomerEntity customer;
}
