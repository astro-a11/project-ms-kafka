package com.wu.assessment.payments.producer;

public interface EventProducer {

  void produceMessage(Long key, String value);
}
