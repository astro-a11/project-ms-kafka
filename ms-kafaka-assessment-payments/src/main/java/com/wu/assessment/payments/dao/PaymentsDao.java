package com.wu.assessment.payments.dao;

import org.springframework.data.repository.CrudRepository;
import com.wu.assessment.payments.eo.PaymentsEntity;

public interface PaymentsDao extends CrudRepository<PaymentsEntity, Long>{

}
