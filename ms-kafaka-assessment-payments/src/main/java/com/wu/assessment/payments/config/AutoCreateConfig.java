package com.wu.assessment.payments.config;

import org.apache.kafka.clients.admin.NewTopic;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.kafka.config.TopicBuilder;

@Configuration
public class AutoCreateConfig {

  @Bean
  public NewTopic libraryEvents() {

    return TopicBuilder.name("addCountPaymentsTopic").partitions(3).build();
  }
}
