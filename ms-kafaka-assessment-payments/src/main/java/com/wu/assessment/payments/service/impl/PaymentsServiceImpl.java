package com.wu.assessment.payments.service.impl;

import lombok.AllArgsConstructor;
import org.apache.kafka.common.errors.ResourceNotFoundException;
import org.springframework.stereotype.Service;
import com.wu.assessment.payments.dao.PaymentsDao;
import com.wu.assessment.payments.eo.PaymentsEntity;
import com.wu.assessment.payments.model.CreditCard;
import com.wu.assessment.payments.model.Payments;
import com.wu.assessment.payments.producer.EventProducer;
import com.wu.assessment.payments.service.PaymentsService;

@Service
@AllArgsConstructor
public class PaymentsServiceImpl implements PaymentsService {

  private final PaymentsDao dao;
  private final EventProducer producer;

  @Override
  public Payments save(Payments payment) {

    PaymentsEntity entity = dao.save(PaymentsEntity.builder()
        .cardNumber(payment.getCreditcard().getCardNumber())
        .idCustomer(payment.getIdCustomer())
        .cvv(payment.getCreditcard().getCvv())
        .expiryDate(payment.getCreditcard().getExpiryLocalDate())
        .name(payment.getCreditcard().getName()).build());

    payment.setIdPayment(entity.getIdPayment());

    producer.produceMessage(entity.getIdCustomer(), payment.toString());
    return payment;
  }

  @Override
  public Payments getById(Long idPayment) {

    PaymentsEntity entity = dao.findById(idPayment)
        .orElseThrow(() -> new ResourceNotFoundException(
            "We can't find the resource with Id "
                .concat(idPayment.toString())));

    return Payments.builder().idCustomer(entity.getIdCustomer())
        .idPayment(entity.getIdPayment())
        .creditcard(CreditCard.builder().cardNumber(entity.getCardNumber())
            .cvv(entity.getCvv()).expiryLocalDate(entity.getExpiryDate())
            .name(entity.getName()).build())
        .build();
  }

}
