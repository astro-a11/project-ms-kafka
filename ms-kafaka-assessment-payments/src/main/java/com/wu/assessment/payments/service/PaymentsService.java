package com.wu.assessment.payments.service;

import com.wu.assessment.payments.model.Payments;

public interface PaymentsService {

  public Payments save(Payments payment);
  
  public Payments getById(Long idPayment);
  
}
