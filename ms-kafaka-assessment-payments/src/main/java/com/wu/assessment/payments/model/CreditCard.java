package com.wu.assessment.payments.model;

import java.time.LocalDate;
import java.time.format.DateTimeFormatter;
import javax.validation.constraints.Future;
import javax.validation.constraints.Max;
import javax.validation.constraints.NotBlank;
import javax.validation.constraints.NotNull;
import lombok.Builder;
import lombok.Data;
import com.fasterxml.jackson.annotation.JsonIgnore;

@Data
@Builder
public class CreditCard {

  private static final DateTimeFormatter DATE_FORMAT =
      DateTimeFormatter.ofPattern("MM/yyyy");

  @NotBlank
  private String name;
  @NotNull
  @Max(value = 9999999999999999L)
  private Long cardNumber;
  @Future
  @JsonIgnore
  private LocalDate expiryLocalDate;
  private String expiryDate;
  @NotNull
  @Max(value = 999)
  private Short cvv;

  public void setExpiryDate(String expiryDate) {
    this.expiryDate = expiryDate;
    this.expiryLocalDate = LocalDate.parse(expiryDate, DATE_FORMAT);
  }

  public void setExpiryLocalDate(LocalDate expiryLocalDate) {
    this.expiryDate = expiryLocalDate.format(DATE_FORMAT);
    this.expiryLocalDate = expiryLocalDate;
  }

}
