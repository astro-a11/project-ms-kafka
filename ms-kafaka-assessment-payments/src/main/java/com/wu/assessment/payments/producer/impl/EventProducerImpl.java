package com.wu.assessment.payments.producer.impl;

import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.kafka.core.KafkaTemplate;
import org.springframework.kafka.support.SendResult;
import org.springframework.stereotype.Component;
import org.springframework.util.concurrent.ListenableFuture;
import org.springframework.util.concurrent.ListenableFutureCallback;
import com.wu.assessment.payments.producer.EventProducer;

@Component
@Slf4j
public class EventProducerImpl implements EventProducer {

  @Autowired
  private KafkaTemplate<Long, String> kafkaTemplate;

  public void produceMessage(Long key, String value) {

    ListenableFuture<SendResult<Long, String>> listeneableFuture =
        kafkaTemplate.sendDefault(key, value);

    listeneableFuture
        .addCallback(new ListenableFutureCallback<SendResult<Long, String>>() {

          @Override
          public void onSuccess(SendResult<Long, String> result) {

            log.info(
                "Message was successfully sended for key : {}  and value : {} on partition {}",
                key, value, result.getRecordMetadata().partition());

          }

          @Override
          public void onFailure(Throwable excep) {

            log.error("Error sending the message, the exception is {}", excep);
            throw new InternalError();

          }
        });

  }
}
