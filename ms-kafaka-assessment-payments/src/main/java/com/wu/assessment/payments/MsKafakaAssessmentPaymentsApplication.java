package com.wu.assessment.payments;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class MsKafakaAssessmentPaymentsApplication {

	public static void main(String[] args) {
		SpringApplication.run(MsKafakaAssessmentPaymentsApplication.class, args);
	}

}
