# Description

This repository consist on 2 MicroServices: [ms-kafaka-assessment-customer](https://gitlab.com/astro-a11/project-ms-kafka/-/tree/main/ms-kafaka-assessment-customer) and [ms-kafaka-assessment-payments](https://gitlab.com/astro-a11/project-ms-kafka/-/tree/main/ms-kafaka-assessment-payments) 

<details><summary>Runing Kafka Broker</summary>
- Install Kafka and start as a normal way, you don't need to create any topic, For exercise purposes SpringBoot MicroService will take care about that.

## Start Kafka
- First you must to start Zookeper.

```bash
.\zookeeper-server-start.bat .\..\..\config\zookeeper.properties
```
- Now yo can start kafka.

```bash
.\kafka-server-start.bat ..\..\config\server.properties
```
MicroService will create only one kafka broker with topic named #addCountPaymentsTopic with 3 partitions once you run it.
</details>

<details><summary>Set Up MariaDB</summary>

- You can install any way MariaDB but we are going to do it through [MariaDB Docker image from Docker Hub](https://hub.docker.com/_/mariadb).

## Generate and Run a Container
- First we are going to create our own Docker Network.
```bash
docker network rm lab
```
- We'll pull MariaDB image (in case you don't have it already), create a container from this image and start it.
```bash
docker run --rm --net lab -p 3306:3306 --name db01-ms-kafka-api -e MYSQL_ROOT_PASSWORD=sasa -d mariadb
```
- Now, we are going to create the data base, you must to navigate in docker directory contained in this project.
```bash
docker exec -i db01-ms-kafka-api sh -c 'exec mysql -uroot -p"$MYSQL_ROOT_PASSWORD"' < dump/customers.sql
```
</details>

<details><summary>Set Up ms-kafaka-assessment-customer</summary>
- You must to navigate inside project directory.

## Generate Maven Artifact

-   You must to download all the dependencies from a repository, in this case Maven Repository, and generate the jar file with next command on bash.

```bash
mvn clean install
```
## Start SpringBoot Application
-   Start up the ms-kafaka-assessment-customer.

```bash
spring-boot:run
```
</details>

<details><summary>ms-kafaka-assessment-payments</summary>
- You must to navigate inside project directory.
## Generate Maven Artifact

-   You must to download all the dependencies from a repository, in this case Maven Repository, and generate the jar file with next command on bash.

```bash
mvn clean install
```
## Start SpringBoot Application

-   Start up the ms-kafaka-assessment-customer.

```bash
spring-boot:run
```
</details>
