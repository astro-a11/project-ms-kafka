package com.wu.assessment.customer.eo;

import java.util.List;
import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.OneToMany;
import javax.persistence.OneToOne;
import javax.persistence.Table;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;
@Data
@Builder
@NoArgsConstructor
@AllArgsConstructor
@Entity
@Table(name = "CUSTOMER")
public class CustomerEntity {

  @Id
  @GeneratedValue(strategy = GenerationType.IDENTITY)
  @Column(name = "ID_CUSTOMER")
  private Long idCustomer;

  private String name;
  private String address;
  private Long phone;
  private String email;
  @Column(name = "PAYMENT_COUNT")
  private Long paymentCount;

  @OneToOne(cascade = {CascadeType.ALL})
  @JoinColumn(name = "ID_RECEIVER", referencedColumnName = "ID_RECEIVER")
  private ReceiverEntity receiver;

  @OneToMany(mappedBy = "customer", cascade = {CascadeType.REMOVE})
  private List<PaymentsEntity> payment;

}
