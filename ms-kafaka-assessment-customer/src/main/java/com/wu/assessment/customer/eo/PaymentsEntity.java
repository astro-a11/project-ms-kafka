package com.wu.assessment.customer.eo;

import java.time.LocalDate;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;


@Data
@Builder
@NoArgsConstructor
@AllArgsConstructor
@Entity
@Table(name = "PAYMENT")
public class PaymentsEntity {

  @Id
  @GeneratedValue(strategy = GenerationType.IDENTITY)
  @Column(name = "ID_PAYMENT")
  private Long idPayment;

  @ManyToOne(fetch = FetchType.LAZY)
  @JoinColumn(name = "ID_CUSTOMER", nullable = false)
  private CustomerEntity customer;

  @Column(name = "NAME")
  private String name;
  @Column(name = "CARD_NUMBER")
  private Long cardNumber;
  @Column(name = "EXPIRY_DATE")
  private LocalDate expiryDate;
  @Column(name = "CVV")
  private Short cvv;

}
