package com.wu.assessment.customer.model;

import javax.validation.Valid;
import javax.validation.constraints.NotBlank;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Pattern;
import lombok.AccessLevel;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;
import lombok.Setter;
import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;

@Data
@AllArgsConstructor
@NoArgsConstructor
@Builder
public class Customer {

  private Long idCustomer;

  @NotBlank
  private String name;
  @NotBlank
  private String address;
  @NotNull
  private Long phone;
  @Pattern(regexp = "^[A-Z0-9._%+-]+@[A-Z0-9.-]+\\\\.[A-Z]{2,6}$")
  private String email;
  @Valid
  @NotNull
  private Receiver receiver;
  @Setter(AccessLevel.NONE)
  @Builder.Default
  private Long paymentCount = 0L;

  @Override
  public String toString() {

    try {
      return new ObjectMapper().writeValueAsString(this);
    } catch (JsonProcessingException exc) {
      return "";
    }
  }
}
