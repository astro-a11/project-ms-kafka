package com.wu.assessment.customer.dao;

import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;
import org.springframework.data.repository.query.Param;
import org.springframework.transaction.annotation.Transactional;
import com.wu.assessment.customer.eo.CustomerEntity;

public interface CustomerDao extends CrudRepository<CustomerEntity, Long> {

  @Modifying
  @Query(
      value = "update CustomerEntity c set c.paymentCount = (c.paymentCount + 1) where c.idCustomer = :idCustomer")
  @Transactional
  void addPaymentCount(@Param(value = "idCustomer") Long idCustomer);
}
