package com.wu.assessment.customer.model;

import javax.validation.constraints.NotBlank;

import lombok.Builder;
import lombok.Data;

@Data
@Builder
public class Receiver {

    @NotBlank
    private String name;
    @NotBlank
    private String address;
    
}
