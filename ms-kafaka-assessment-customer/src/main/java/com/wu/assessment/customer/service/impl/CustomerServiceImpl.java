package com.wu.assessment.customer.service.impl;

import lombok.AllArgsConstructor;
import org.apache.kafka.common.errors.ResourceNotFoundException;
import org.springframework.stereotype.Service;
import com.wu.assessment.customer.dao.CustomerDao;
import com.wu.assessment.customer.eo.CustomerEntity;
import com.wu.assessment.customer.eo.ReceiverEntity;
import com.wu.assessment.customer.model.Customer;
import com.wu.assessment.customer.model.Receiver;
import com.wu.assessment.customer.service.CustomerService;

@AllArgsConstructor
@Service
public class CustomerServiceImpl implements CustomerService {

  private final CustomerDao dao;

  @Override
  public Customer save(Customer customer) {

    CustomerEntity entity = CustomerEntity.builder().name(customer.getName())
        .address(customer.getAddress()).email(customer.getEmail())
        .phone(customer.getPhone())
        .receiver(ReceiverEntity.builder()
            .address(customer.getReceiver().getAddress())
            .name(customer.getReceiver().getName()).build())
        .paymentCount(customer.getPaymentCount()).build();

    customer.setIdCustomer(dao.save(entity).getIdCustomer());
    return customer;
  }

  @Override
  public Customer getById(Long idCustomer) {

    CustomerEntity entity = dao.findById(idCustomer)
        .orElseThrow(() -> new ResourceNotFoundException(
            "We can't find the resource with Id "
                .concat(idCustomer.toString())));

    return Customer.builder().address(entity.getAddress())
        .email(entity.getEmail()).idCustomer(entity.getIdCustomer())
        .name(entity.getName()).phone(entity.getPhone())
        .paymentCount(entity.getPaymentCount())
        .receiver(Receiver.builder().name(entity.getReceiver().getName())
            .address(entity.getReceiver().getAddress()).build())
        .build();
  }

  @Override
  public void addPaymentCount(Long idCustomer) {

    dao.addPaymentCount(idCustomer);

  }

}
