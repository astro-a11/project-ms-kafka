package com.wu.assessment.customer;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class MsKafakaAssessmentCustomerApplication {

	public static void main(String[] args) {
		SpringApplication.run(MsKafakaAssessmentCustomerApplication.class, args);
	}

}
