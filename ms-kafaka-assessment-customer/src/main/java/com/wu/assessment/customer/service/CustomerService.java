package com.wu.assessment.customer.service;

import com.wu.assessment.customer.model.Customer;

public interface CustomerService {

  public Customer save(Customer customer);

  public Customer getById(Long idPayment);
  
  public void addPaymentCount(Long idCustomer);
}
