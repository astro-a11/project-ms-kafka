/*
 * Copyright 2021 the original author or authors.
 *
 * Licensed under the Apache License, Version 2.0 (the "License"); you may not use this file except
 * in compliance with the License. You may obtain a copy of the License at
 *
 * https://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software distributed under the License
 * is distributed on an "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express
 * or implied. See the License for the specific language governing permissions and limitations under
 * the License.
 */
package com.wu.assessment.customer.controller;

import javax.validation.Valid;
import lombok.AllArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import com.wu.assessment.customer.model.Customer;
import com.wu.assessment.customer.service.CustomerService;

@RestController
@RequestMapping("/api/v1/customer")
@AllArgsConstructor
@Slf4j
public class CustomerController {

  private final CustomerService service;

  @PostMapping
  public ResponseEntity<Customer> createCustomer(
      @RequestBody @Valid Customer customer) {

    log.info("request : {}", customer);
    service.save(customer);
    return ResponseEntity.status(HttpStatus.CREATED).body(customer);
  }

  @GetMapping("/{idCustomer}")
  public ResponseEntity<Customer> getCustomer(@PathVariable Long idCustomer) {

    return ResponseEntity.status(HttpStatus.OK)
        .body(service.getById(idCustomer));
  }

}
