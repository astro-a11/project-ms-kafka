package com.wu.assessment.customer.consumer;

import lombok.AllArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.apache.kafka.clients.consumer.ConsumerRecord;
import org.springframework.kafka.annotation.KafkaListener;
import org.springframework.stereotype.Component;
import com.wu.assessment.customer.service.CustomerService;

@Component
@Slf4j
@AllArgsConstructor
public class EventConsumer {

  private final CustomerService service;

  @KafkaListener(topics = {"addCountPaymentsTopic"})
  public void getMessage(ConsumerRecord<Long, String> record) {

    log.info("Message received : {}", record);

    service.addPaymentCount(record.key());

  }
}
